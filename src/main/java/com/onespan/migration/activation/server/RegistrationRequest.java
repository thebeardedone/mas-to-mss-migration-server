package com.onespan.migration.activation.server;

public class RegistrationRequest {

    private String serialNumber;
    private String otp;

    RegistrationRequest() {}

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
