package com.onespan.migration.activation.server.client;

import com.onespan.migration.activation.server.Utils;
import com.vasco.identikey.controller.ConfigurationBean;
import com.vasco.identikey.controller.administration.AdministrationBean;
import com.vasco.identikey.controller.administration.AdministrationCommandResponse;
import com.vasco.identikey.controller.administration.handler.digipass.DigipassQueryResponse;
import com.vasco.identikey.controller.administration.handler.digipass.DigipassSearchParams;
import com.vasco.identikey.controller.provisioning.ProvisioningBean;
import com.vasco.identikey.model.*;
import com.vasco.identikeyserver.identikeytypes.basictypes.UserInput;
import com.vasco.identikeyserver.identikeytypes.provisioning.DsappSRPRegisterResponse;
import com.vasco.identikeyserver.identikeytypes.provisioningtypes.RegisterCredentialInput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OASClient {

    private static OASClient instance;
    final static private Logger logger = LogManager.getLogger(OASClient.class);

    private ConfigurationBean configurationBean;
    private AdministrationBean administrationBean;

    public OASClient(String soapURL) {
        configurationBean = new ConfigurationBean();
        configurationBean.setPrimarySoapURL(soapURL);
        administrationBean = new AdministrationBean(configurationBean);
    }

    public static OASClient getInstance() {
        if(instance == null)
            instance = new OASClient(OASProperties.getInstance().getServerDetails().getUrl());
        return instance;
    }

    boolean performAdminLogon(String domain, String userId, String password) {
        AdministrationCommandResponse administrationCommandResponse = administrationBean.logon(domain, userId, null, null, password, Credentials.RequestHostCode.No);
        if(administrationCommandResponse.getStatusCode() != 0 || administrationCommandResponse.getReturnCode() != 0) {
            logger.error("Failed to perform administrative logon for userId: " + userId);
            Utils.printError(administrationCommandResponse);
            return false;
        }
        return true;
    }

    public boolean isAdminSessionAlive() {
        return administrationBean.isSessionAlive();
    }

    public boolean performAdminLogon(ServerDetails serverDetails) {
        return performAdminLogon(serverDetails.getDomain(), serverDetails.getUserId(), serverDetails.getPassword());
    }

    public boolean performAdminLogon() {
        return performAdminLogon(OASProperties.getInstance().getServerDetails());
    }

    public void performAdminLogoff() {
        AdministrationCommandResponse administrationCommandResponse = administrationBean.logoff();
        if(administrationCommandResponse.getStatusCode() != 0 || administrationCommandResponse.getReturnCode() != 0) {
            logger.error("Failed to perform administrative logoff");
            Utils.printError(administrationCommandResponse);
        }
    }

    public Digipass getDigipass(String serialNumber) throws Exception {
        DigipassSearchParams digipassSearchParams = new DigipassSearchParams();
        digipassSearchParams.setSerialNumber(serialNumber);
        DigipassQueryResponse digipassQueryResponse = administrationBean.getDigipassHandler().search(digipassSearchParams);
        Utils.checkErrorIdentikeyResponse(digipassQueryResponse, true);
        if(digipassQueryResponse.getResults().size() == 0)
            throw new OASNotFoundException();
        return digipassQueryResponse.getResults().get(0);
    }

    public DsappSRPRegisterResponse performDSAPPSRPRegister(String componentType, String domain, String userID, String otp) {
        UserInput userInput = new UserInput();
        userInput.setDomain(domain);
        userInput.setUserID(userID);

        RegisterCredentialInput registerCredentialInput = new RegisterCredentialInput();
        registerCredentialInput.setStaticPassword(otp);

        return new ProvisioningBean(configurationBean).dsappSRPRegister(componentType, userInput, registerCredentialInput);
    }
}
