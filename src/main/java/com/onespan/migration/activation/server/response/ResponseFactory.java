package com.onespan.migration.activation.server.response;

import com.vasco.identikey.controller.IdentikeyError;
import com.vasco.identikeyserver.identikeytypes.provisioning.DsappSRPRegisterResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ResponseFactory {

    final static private Logger logger = LogManager.getLogger(ResponseFactory.class);

    public static GenericResponse CreateInvalidDataResponse() {
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.getResultCodes().setReturnCode(-1);
        genericResponse.getResultCodes().setReturnCodeEnum("RET_FAILURE");
        genericResponse.getResultCodes().setStatusCode(-17);
        genericResponse.getResultCodes().setStatusCodeEnum("STAT_INVDATA");

        return genericResponse;
    }

    public static GenericResponse CreateInternalServerError() {
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.getResultCodes().setReturnCode(-1);
        genericResponse.getResultCodes().setReturnCodeEnum("RET_FAILURE");
        genericResponse.getResultCodes().setStatusCode(-400);
        genericResponse.getResultCodes().setStatusCodeEnum("STAT_NOTAVAIL");

        return genericResponse;
    }

    public static GenericResponse CreateNotFoundError() {
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.getResultCodes().setReturnCode(-1);
        genericResponse.getResultCodes().setReturnCodeEnum("RET_FAILURE");
        genericResponse.getResultCodes().setStatusCode(-13);
        genericResponse.getResultCodes().setStatusCodeEnum("STAT_NOT_FOUND");

        return genericResponse;
    }

    public static GenericResponse CreateRegistrationResponse(DsappSRPRegisterResponse dsappsrpRegisterResponse) {
        GenericResponse registrationResponse = new GenericResponse();
        registrationResponse.getResultCodes().setReturnCode(dsappsrpRegisterResponse.getStatus().getResultCodes().getReturnCode());
        registrationResponse.getResultCodes().setReturnCodeEnum(dsappsrpRegisterResponse.getStatus().getResultCodes().getReturnCodeEnum().value());
        registrationResponse.getResultCodes().setStatusCode(dsappsrpRegisterResponse.getStatus().getResultCodes().getStatusCode());
        registrationResponse.getResultCodes().setStatusCodeEnum(dsappsrpRegisterResponse.getStatus().getResultCodes().getStatusCodeEnum().value());

        if(registrationResponse.getResultCodes().getReturnCode() == 0 && registrationResponse.getResultCodes().getStatusCode() == 0) {
            RegistrationResponseResults registrationResponseResults = new RegistrationResponseResults();
            registrationResponseResults.setRegistrationId(dsappsrpRegisterResponse.getResult().getRegistrationID());
            registrationResponseResults.setActivationPassword(dsappsrpRegisterResponse.getResult().getActivationPassword());
            registrationResponse.setResult(registrationResponseResults);
        } else {
            logger.error("Status Code: " + registrationResponse.getResultCodes().getStatusCode() + " Return Code: " + registrationResponse.getResultCodes().getReturnCode());
            logger.error("Status Code Enum: " + registrationResponse.getResultCodes().getStatusCodeEnum() + " Return Code Enum: " + registrationResponse.getResultCodes().getReturnCodeEnum());
        }

        return registrationResponse;
    }
}
