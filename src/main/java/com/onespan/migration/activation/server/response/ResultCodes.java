package com.onespan.migration.activation.server.response;

public class ResultCodes {

    private String returnCodeEnum;
    private String statusCodeEnum;
    private int returnCode;
    private int statusCode;

    ResultCodes() {}

    public String getReturnCodeEnum() {
        return returnCodeEnum;
    }

    public void setReturnCodeEnum(String returnCodeEnum) {
        this.returnCodeEnum = returnCodeEnum;
    }

    public String getStatusCodeEnum() {
        return statusCodeEnum;
    }

    public void setStatusCodeEnum(String statusCodeEnum) {
        this.statusCodeEnum = statusCodeEnum;
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
