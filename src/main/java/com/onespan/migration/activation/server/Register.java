package com.onespan.migration.activation.server;

import com.onespan.migration.activation.server.client.OASClient;
import com.onespan.migration.activation.server.client.OASNotFoundException;
import com.onespan.migration.activation.server.client.OASProperties;
import com.onespan.migration.activation.server.client.ServerDetails;
import com.onespan.migration.activation.server.response.GenericResponse;
import com.onespan.migration.activation.server.response.ResponseFactory;
import com.vasco.identikey.model.Digipass;
import com.vasco.identikeyserver.identikeytypes.provisioning.DsappSRPRegisterResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Register {

    final static private Logger logger = LogManager.getLogger(Register.class);

    @PostMapping("/register")
    public ResponseEntity<GenericResponse> execute(@RequestBody RegistrationRequest registrationRequest) {

        logger.debug("received register request");
        if (Utils.isEmpty(registrationRequest.getSerialNumber()) || registrationRequest.getSerialNumber().contains("*") || Utils.isEmpty(registrationRequest.getOtp()) || !registrationRequest.getOtp().matches("[0-9]+")) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseFactory.CreateInvalidDataResponse());
        }
        logger.debug("validated registration request parameters");

        if(OASProperties.getInstance() == null) {
            logger.error("failed to retrieve OAS properties");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ResponseFactory.CreateInternalServerError());
        }
        ServerDetails serverDetails = OASProperties.getInstance().getServerDetails();
        OASClient oasClient = OASClient.getInstance();

        if(!oasClient.isAdminSessionAlive()) {
            logger.debug("attempting OAS admin logon");
            boolean success = oasClient.performAdminLogon();
            if (!success) {
                logger.debug("failed to perform admin logon");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ResponseFactory.CreateInternalServerError());
            }
            logger.debug("successfully performed OAS admin logon");
        }

        Digipass digipass;

        try {
            logger.debug("attempting to query DIGIPASS with serial number: " + registrationRequest.getSerialNumber());
            digipass = oasClient.getDigipass(registrationRequest.getSerialNumber());
            logger.debug("successfully retrieved DIGIPASS with serial number: " + registrationRequest.getSerialNumber());
        } catch (OASNotFoundException exception) {
            logger.warn("failed to find DIGIPASS with serial number: " + registrationRequest.getSerialNumber());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ResponseFactory.CreateNotFoundError());
        } catch (Exception exception) {
            logger.error("failed to query DIGIPASS with serial number: " + registrationRequest.getSerialNumber());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ResponseFactory.CreateInternalServerError());
        }

        if(Utils.isEmpty(digipass.getAssignedUserID())) {
            logger.warn("retrieved DIGIPASS is not assigned to a user");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseFactory.CreateInvalidDataResponse());
        }

        logger.debug("attempting DSAPP-SRP Register for user: " + digipass.getAssignedUserID());
        DsappSRPRegisterResponse dsappsrpRegisterResponse = oasClient.performDSAPPSRPRegister(serverDetails.getProvisioningComponent(), digipass.getDomain(), digipass.getAssignedUserID(), registrationRequest.getOtp());
        logger.debug("successfully performed DSAPP-SRP Register for user: " + digipass.getAssignedUserID());

        return ResponseEntity.status(HttpStatus.OK).body(ResponseFactory.CreateRegistrationResponse(dsappsrpRegisterResponse));
    }
}
