package com.onespan.migration.activation.server.response;

public class GenericResponse {
    private ResultCodes resultCodes = new ResultCodes();
    private RegistrationResponseResults result = new RegistrationResponseResults();

    public GenericResponse() {}

    public ResultCodes getResultCodes() {
        return resultCodes;
    }

    public void setResultCodes(ResultCodes resultCodes) {
        this.resultCodes = resultCodes;
    }

    public RegistrationResponseResults getResult() {
        return result;
    }

    public void setResult(RegistrationResponseResults result) {
        this.result = result;
    }
}
