package com.onespan.migration.activation.server.response;

public class RegistrationResponseResults {

    private String registrationId;
    private String activationPassword;

    RegistrationResponseResults() {}

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getActivationPassword() {
        return activationPassword;
    }

    public void setActivationPassword(String activationPassword) {
        this.activationPassword = activationPassword;
    }
}
