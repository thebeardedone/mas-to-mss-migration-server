package com.onespan.migration.activation.server;

import com.onespan.migration.activation.server.client.OASProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class ServerApplication {

	public static void main(String[] args) {
		OASProperties.getInstance();
		SpringApplication.run(ServerApplication.class, args);
	}

}
