# Getting Started

The following is a sample server which uses the OAS Soap Wrappers in order to lookup a userId using a DIGIPASS serial 
number and performs a DSAPP-SRP Register call in order to generate activation credentials.

## Sample Request and Server Responses

The server will process a POST JSON request on the `/register` endpoint. For example 
https://<server-ip>:\<port>/<deployment_path>/register

```json
{
	"serialNumber": "VDS1000000",
	"otp": "000000"
}
```

Upon validation, the server will respond with a JSON response containing the activation credentials for a mobile device
to continue with the DSAPP-SRP provisioning process.

```json
{
    "resultCodes": {
        "returnCodeEnum": "RET_SUCCESS",
        "statusCodeEnum": "STAT_SUCCESS",
        "returnCode": 0,
        "statusCode": 0
    },
    "result": {
        "registrationId": "XXXXXXXX",
        "activationPassword": "YYYYYYYY"
    }
}
```

If OAS returns an error, the returnCodeEnum, statusCodeEnum, returnCode and statusCode are forwarded in the response to
the caller. The OAS forwarded responses will always return with HTTP status 200 (refer to the OAS SOAP
reference for more information on response codes). The following additional errors may be returned by the server:

1. Request fails basic parameter validation (ex. a field is blank) thus resulting in an error 400.

    ```json
    {
        "resultCodes": {
            "returnCodeEnum": "RET_FAILURE",
            "statusCodeEnum": "STAT_INVDATA",
            "returnCode": -1,
            "statusCode": -17
        },
        "result": {
            "registrationId": null,
            "activationPassword": null
        }
    }
    ```

2. Server fails to read OAS properties file (only done on initial request), Administrative Logon command or DIGIPASS 
query command fails.

    ```json
    {
        "resultCodes": {
            "returnCodeEnum": "RET_FAILURE",
            "statusCodeEnum": "STAT_NOTAVAIL",
            "returnCode": -1,
            "statusCode": -400
        },
        "result": {
            "registrationId": null,
            "activationPassword": null
        }
    }
    ```

3. DIGIPASS query command returns an empty result when attempting to resolve the userId.

    ```json
    {
        "resultCodes": {
            "returnCodeEnum": "RET_FAILURE",
            "statusCodeEnum": "STAT_NOT_FOUND",
            "returnCode": -1,
            "statusCode": -13
        },
        "result": {
            "registrationId": null,
            "activationPassword": null
        }
    }
    ```

## Build

In order to build the application you need to have maven installed and you will also need to have the OAS SOAP wrappers
and client. These can be provided upon request by the OneSpan Professional Services department.

First you will need to install the SOAP wrappers and client to your local maven repository:

```
mvn install:install-file -Dfile=IdentikeySOAPClient.jar -DgroupId=com.onespan.oas -DartifactId=soapclient -Dversion=3.18.0 -Dpackaging=jar

mvn install:install-file -Dfile=IdentikeyWrapper.jar -DgroupId=com.onespan.oas -DartifactId=soapwrapper -Dversion=3.18.0 -Dpackaging=jar
```

Then you will be able to run package in order to generate the war file:

```
mvn package
```

The war file will be present in the generated target directory.

## Deployment

In order to deploy the war file you will need an application server (ex. Tomcat). Rename the war file to a user friendly
name ex. server.war (to ensure that the deployment path is a nice string) and place the file into the webapps directory.

In tomcat you will need to configure a runtime property:

```
-Dcom.onespan.server.config.file=<path_to_oas.properties>
```

This properties file must contain the following properties:

```
# OAS Server URL ex. https://<ip-address>:8888
server.url=
server.admin.userid=
server.admin.domain=
server.admin.password=

# MDL provisioning client component name for DSAPP-SRP Register
server.provisioning.component=
```

If you would like to deploy this war file on the same tomcat as the DIGIPASS Gateway on linux you can modify the init.d
service file, place the oas.properties file in the DIGIPASS Gateway root path and add the following to the JAVA_OPTS:

```
JAVA_OPTS="$JAVA_OPTS -Dcom.onespan.server.config.file=$ROOT_PATH/oas.properties"
```

Remember to refresh your service files and then restart the service.

On Windows add the following property to the catalina properties file 
(default location `C:\Program Files\OneSpan\Digipass Gateway\tomcat\conf\catalina.properties`):

```
com.onespan.server.config.file=
```

WARNING: remember to not put the directory in quotes when setting the property.

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.1.BUILD-SNAPSHOT/maven-plugin/)

